---
title: "About berries"
---

# About berries

Berries are small fruits. They come in all shapes and sizes. Scientists define 
berries as "a fruit produced from the ovary of a single flower in which the 
outer layer of the ovary wall develops into an edible fleshy portion (pericarp)."

There are a number of fruits that you wouldn't normally think of as berries, 
such as:

- bananas
- tomatoes
- cucumbers
- grapes

## Cooking with berries

There are many  of berries, but not all berries are created equal. Some 
are better eaten fresh while others are best made into pies and preserves.

Here are a few true berries that make great pies and preserves:

- gooseberrys
- blueberries
- lingonberries

## Aggregate fruts: berry good impostors

Did you  that raspberries, straberries and blackberrys are technically not 
berries? In fact, these are actually aggregate fruts. 

## Reference

This article references and quotes the [Berry](https://en.wikipedia.org/wiki/Berry) 
page in Wikipedia.